#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    if 'DJANGO_SETTINGS_MODULE' not in os.environ:
        from django.core.exceptions import ImproperlyConfigured
        raise ImproperlyConfigured('DJANGO_SETTINGS_MODULE not found in environment')

    from django.core.management import execute_from_command_line

    args = sys.argv[:]
    if len(args) == 2 and sys.argv[1] == 'runserver':
        # Default to serving externally if not told otherwise
        args = sys.argv + ['0.0.0.0:10080']

    execute_from_command_line(args)
