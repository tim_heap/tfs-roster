# Django settings for tfsroster project.
from os import path
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS

# Path setup:
# / - Project root - PROJECT_ROOT
#   tfsroster/ - The project source code - SRC_ROOT
#   var/ - Variable, non-version-controlled stuff - VAR_ROOT
#       www/ - Web accessible content - WWW_ROOT
#           static/ - Static files - STATIC_ROOT
#           media/ - User uploaded content - MEDIA_ROOT


# URL structure
WWW_URL = '/assets/'
MEDIA_URL = WWW_URL
STATIC_URL = path.join(WWW_URL, 'static/')
FILEMANAGER_UPLOAD_URL = MEDIA_URL


INSTALLED_APPS = (
    # Apps from this project
    'tfsroster',
    'tfsroster.frontend',
    'tfsroster.people',
    'tfsroster.schedules',
    'tfsroster.users',

    # Third party apps
    'adminextensions',
    'filemanager',
    'django_template_media',

    # Django apps
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
)


MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    'django_template_media.context_processors.template_media',
)


# Root project files
BASE_APP_NAME = 'tfsroster'
ROOT_URLCONF = BASE_APP_NAME + '.urls'
WSGI_APPLICATION = BASE_APP_NAME + '.wsgi.application'


# Built in Australia. Change this in local.py if you want it different
TIME_ZONE = 'Australia/Hobart'
LANGUAGE_CODE = 'en-au'


# Get set up for localisation
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Put all static files and templates in apps
STATICFILES_FINDERS = ['django.contrib.staticfiles.finders.AppDirectoriesFinder']
TEMPLATE_LOADERS = ['django.template.loaders.app_directories.Loader']


AUTH_USER_MODEL = 'users.User'
LOGIN_URL = 'login'
LOGIN_REDIRECT_URL = '/'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


DATE_FORMAT = '%Y-%m-%d'

TEST_RUNNER = 'django.test.runner.DiscoverRunner'


from django.contrib import messages
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'
MESSAGE_TAGS = {
    messages.DEBUG: 'debug',
    messages.INFO: 'info',
    messages.SUCCESS: 'good',
    messages.WARNING: 'warn',
    messages.ERROR: 'evil',
}
