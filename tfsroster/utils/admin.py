from django.contrib.admin import ModelAdmin
from filemanager.models import FileBrowserField


class ExtraFieldsAdmin(ModelAdmin):
    def formfield_for_dbfield(self, db_field, request=None, **kwargs):
        if isinstance(db_field, FileBrowserField):
            return db_field.formfield(**kwargs)

        return super(ExtraFieldsAdmin, self).formfield_for_dbfield(
            db_field, request=request, **kwargs)
