from __future__ import absolute_import, unicode_literals

from django import template
from django.utils.text import force_text


register = template.Library()


def add_css_class(class_name, class_list):
    if not class_list:
        return class_name
    return class_list + ' ' + class_name


@register.filter
def add_class(bound_field, class_name):
    widget = bound_field.field.widget
    class_list = widget.attrs.get('class')
    widget.attrs['class'] = add_css_class(class_name, class_list)
    return bound_field


@register.filter
def data(bound_field, data):
    try:
        key, value = data.split('=', 1)
    except ValueError:
        key, value = data, ''
    widget = bound_field.field.widget
    widget.attrs['data-' + key] = value
    return bound_field


@register.inclusion_tag("frontend/tags/field.html", takes_context=True)
def field(context, bound_field):
    add_class(bound_field, "form-control")
    return {
        'field': bound_field,
        'errors': bound_field.errors,
    }
