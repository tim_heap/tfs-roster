import json

from django import template

register = template.Library()


@register.filter
def can_manage_station(user, station):
    return user.can_manage_station(station)
