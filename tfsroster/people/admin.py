from django import forms
from django.contrib import admin
from djangosuggestions.widgets import SuggestionWidget
from tfsroster.people.models import (
    Station, Truck, Person, Rank, Qualification, PhoneNumber)
from tfsroster.utils.admin import ExtraFieldsAdmin


@admin.register(Station)
class StationAdmin(ExtraFieldsAdmin):
    class TruckInline(admin.TabularInline):
        model = Truck
        extra = 1

    inlines = [TruckInline]

    fieldsets = [
        ('None', {'fields': [
            'number', 'name']})
    ]

    list_display = ('name', 'number')
    list_search = ('number', 'name',)


@admin.register(Rank)
class RankAdmin(ExtraFieldsAdmin):
    pass


@admin.register(Qualification)
class QualificationAdmin(ExtraFieldsAdmin):
    pass


@admin.register(Person)
class PersonAdmin(ExtraFieldsAdmin):
    class PhoneNumberInline(admin.TabularInline):
        class PhoneNumberForm(forms.ModelForm):
            class Meta:
                widgets = {
                    'type': SuggestionWidget(
                        suggestions=PhoneNumber.TYPE_SUGGESTIONS),
                }

        model = PhoneNumber
        form = PhoneNumberForm
        extra = 1

    inlines = [PhoneNumberInline]

    filter_horizontal = ('qualifications',)

    list_display = ('name', 'rank', 'station')
    list_search = ('name')

    list_filter = ('rank', 'station')
