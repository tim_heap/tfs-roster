from django.conf.urls import patterns, include, url
from django.contrib import admin

from . import views

urlpatterns = patterns(
    '',
    url(r'^$',
        views.dashboard,
        name='dashboard'),
    url(r'^station/(?P<station_pk>\d+)/', include(patterns(
        '',
        url(r'^$', views.station_dashboard, name='station-dashboard'),
        url(r'^manage/$', views.station_manage, name='station-manage'),
        url(r'^person/', include(patterns(
            '',
            url(r'^create/$', views.person_create, name='person-create'),
            url(r'^change/(?P<person_pk>\d+)/$', views.person_change, name='person-change'),
            url(r'^remove/(?P<person_pk>\d+)/$', views.person_remove, name='person-remove'),
        ))),
        url(r'', include('tfsroster.schedules.urls')))))
)
