import functools
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, resolve_url
from django.utils.encoding import force_str
from django.utils.six.moves.urllib.parse import urlparse
from .models import Station, Person

def station_view(fn):
    @functools.wraps(fn)
    def view(request, *, station_pk, **kwargs):
        station = get_object_or_404(Station, pk=station_pk)
        return fn(request, station=station, **kwargs)
    return view


def person_station_view(fn):
    @functools.wraps(fn)
    @station_view
    def view(request, *, station, person_pk, **kwargs):
        person = get_object_or_404(station.people, pk=person_pk)
        return fn(request, station=station, person=person, **kwargs)
    return view


def manage_station_required(fn):
    @functools.wraps(fn)
    @login_required
    def view(request, *, station, **kwargs):
        if request.user.can_manage_station(station):
            return fn(request, station=station, **kwargs)
        messages.error(request, 'You do not have permission to view that station')
        return _redirect_to_login(request)
    return view


def _redirect_to_login(request):
    path = request.build_absolute_uri()
    # urlparse chokes on lazy objects in Python 3, force to str
    resolved_login_url = force_str(
        resolve_url(settings.LOGIN_URL))
    # If the login url is the same scheme and net location then just
    # use the path as the "next" url.
    login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
    current_scheme, current_netloc = urlparse(path)[:2]
    if ((not login_scheme or login_scheme == current_scheme) and
            (not login_netloc or login_netloc == current_netloc)):
        path = request.get_full_path()
    from django.contrib.auth.views import redirect_to_login
    return redirect_to_login(path, resolved_login_url)
