from django import forms
from django.contrib.auth.models import Group

from tfsroster.constants import station_manager_group_name
from tfsroster.users.models import User
from tfsroster.users.utils import send_password_reset_email
from .models import Person



class ChangeUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email']

    manager = forms.BooleanField(required=False)

    def __init__(self, *, request=None, **kwargs):
        super().__init__(**kwargs)
        self.manager_group = Group.objects.get(name=station_manager_group_name)
        if self.instance.pk:
            self.initial['manager'] = self.instance.groups.filter(
                pk=self.manager_group.pk).exists()
        self.request = request

    def save(self):
        user = super().save()
        data = self.cleaned_data

        if data['manager']:
            user.groups = [self.manager_group]
        else:
            user.groups = []

        return user


class ChangePersonForm(forms.ModelForm):
    class Meta:
        fields = ['name', 'rank', 'qualifications']
        model = Person

    has_login = forms.BooleanField(required=False)

    def __init__(self, *, data=None, files=None, request=None, **kwargs):
        super().__init__(data=data, files=files, **kwargs)
        self.initial['has_login'] = self.instance.user is not None
        self.request = request
        self.user_form = ChangeUserForm(
            data=data, request=request, instance=self.instance.user,
            prefix=(self.prefix + '-' if self.prefix else '') + 'user')

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('has_login', False):
            self.user_form.full_clean()
        return cleaned_data

    def is_valid(self):
        is_valid = super().is_valid()
        if self.cleaned_data.get('has_login', False):
            is_valid = self.user_form.is_valid() and is_valid
        return is_valid

    def save(self, request):
        person = super().save(commit=False)
        data = self.cleaned_data

        if data['has_login']:
            if person.user is not None:
                # Simple case, just update the user
                self.user_form.save()
            else:
                # User has been created, send them a reset password email
                person.user = self.user_form.save()
                send_password_reset_email(person.user, request)
        else:
            if person.user:
                # Had a user, now do not. Delete the user!
                person.user.delete()
                person.user = None
            else:
                pass # Simple case, nothing to do!

        person.save()
        self.save_m2m()
        return person
