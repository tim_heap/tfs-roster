# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='qualification',
            options={'ordering': ('order', 'name')},
        ),
        migrations.AddField(
            model_name='qualification',
            name='order',
            field=models.PositiveIntegerField(default=100000),
            preserve_default=False,
        ),
    ]
