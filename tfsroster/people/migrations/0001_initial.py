# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filemanager.models
from django.conf import settings
import positions.fields
import featureditem.fields
import django_localflavor_au.models
import djangosuggestions.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'people',
                'ordering': ('rank__order', 'name'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PhoneNumber',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('type', djangosuggestions.models.SuggestionField(max_length=50)),
                ('number', django_localflavor_au.models.AUPhoneNumberField(max_length=10)),
                ('primary', featureditem.fields.FeaturedField(default=False)),
                ('person', models.ForeignKey(related_name='phone_numbers', to='people.Person')),
            ],
            options={
                'ordering': ('-primary',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Qualification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('code', models.CharField(max_length=10)),
                ('icon', filemanager.models.FileBrowserField(upload_to='', null=True, blank=True)),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rank',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('order', positions.fields.PositionField(default=-1)),
                ('icon', filemanager.models.FileBrowserField(upload_to='', null=True, blank=True)),
            ],
            options={
                'ordering': ('order',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Station',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('number', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ['name'],
                'permissions': [('manage_all_station', 'Can manage all stations'), ('manage_own_station', 'Can manage their own station')],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Truck',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('station', models.ForeignKey(to='people.Station')),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='person',
            name='qualifications',
            field=models.ManyToManyField(to='people.Qualification', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='person',
            name='rank',
            field=models.ForeignKey(to='people.Rank'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='person',
            name='station',
            field=models.ForeignKey(related_name='people', to='people.Station'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='person',
            name='user',
            field=models.ForeignKey(blank=True, null=True, unique=True, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
