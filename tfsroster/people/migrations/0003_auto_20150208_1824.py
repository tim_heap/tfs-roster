# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0002_add_qualification_order'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='station',
            options={'ordering': ['name'], 'permissions': [('manage_all_stations', 'Can manage all stations'), ('manage_own_station', 'Can manage their own station')]},
        ),
        migrations.AlterField(
            model_name='person',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, unique=True),
            preserve_default=True,
        ),
    ]
