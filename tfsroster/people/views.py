from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse
from django.db import transaction
from django.shortcuts import redirect, render

from .decorators import station_view, person_station_view, manage_station_required
from .forms import ChangePersonForm
from .models import Station, Person


@login_required
def dashboard(request):
    station_list = Station.objects.for_user(request.user)
    if station_list.count() == 1:
        return redirect(reverse('station-dashboard', kwargs={
            'station_pk': station_list.get().pk}))
    return render(request, 'people/dashboard.html', {
        'station_list': station_list})


@station_view
@manage_station_required
def station_dashboard(request, station):
    return render(request, 'people/station.html', {'station': station})


@station_view
@manage_station_required
def station_manage(request, station):
    people = station.people.all()\
        .select_related('rank')\
        .prefetch_related('qualifications')
    return render(request, 'people/station_manage.html', {
        'station': station, 'people': people})


@permission_required('people.add_person')
@station_view
@manage_station_required
def person_create(request, station):
    person = Person(station=station, user=None)
    if request.method == 'POST':
        form = ChangePersonForm(data=request.POST, instance=person)
        if form.is_valid():
            with transaction.atomic():
                person = form.save(request)
            messages.success(request, '{} has been added'.format(person))
            return redirect(station_manage, station_pk=station.pk)
    else:
        form = ChangePersonForm(instance=person)

    return render(request, 'people/person/create.html', {
        'form': form, 'station': station})


@permission_required('people.change_person')
@person_station_view
@manage_station_required
def person_change(request, station, person):
    if request.method == 'POST':
        form = ChangePersonForm(data=request.POST, instance=person)
        if form.is_valid():
            with transaction.atomic():
                person = form.save(request)
            messages.success(request, '{} has been changed'.format(person))
            return redirect(station_manage, station_pk=station.pk)
    else:
        form = ChangePersonForm(instance=person)

    return render(request, 'people/person/change.html', {
        'form': form, 'station': station, 'person': person})


@permission_required('people.delete_person')
@person_station_view
@manage_station_required
def person_remove(request, station, person):
    if request.method == 'POST':
        if person.user and not person.user.is_superuser:
            person.user.delete()
        person.delete()
        messages.success(request, '{} has been removed'.format(person))
        return redirect(station_manage, station_pk=station.pk)

    return render(request, 'people/person/remove.html', {
        'station': station, 'person': person})
