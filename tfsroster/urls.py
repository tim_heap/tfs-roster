from __future__ import absolute_import, unicode_literals

from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^', include('tfsroster.people.urls')),
    url(r'^account/', include('tfsroster.users.urls')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/filemanager/', include('filemanager.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
