from django.conf.urls import patterns, include, url

from . import views


urlpatterns = patterns(
    '',
    url(r'^login/', views.login, name='login'),
    url(r'^logout/', views.logout, name='logout'),
    url(r'^reset-password/$', views.password_reset, name='password-reset'),
    url(r'^reset-password/set/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.password_reset_set,
        name='password-reset-set'),
)
