from django.core.mail import send_mail
from django.contrib.auth import get_user_model

from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.template import loader
from django.utils.http import int_to_base36

def send_password_reset_email(user, request, use_https=None,
                            token_generator=default_token_generator):
    if use_https is None:
        use_https = request.is_secure()

    current_site = get_current_site(request)
    site_name = current_site.name
    domain = current_site.domain
    c = {
        'email': user.email,
        'domain': domain,
        'site_name': site_name,
        'uid': int_to_base36(user.pk),
        'user': user,
        'token': token_generator.make_token(user),
        'protocol': use_https and 'https' or 'http',
    }
    subject = '[{site_name}] Reset your password'.format(**c)
    subject = ''.join(subject.splitlines())

    email = loader.render_to_string('users/password_reset_email.html', c)
    send_mail(subject, email, None, [user.email])
