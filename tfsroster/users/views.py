from django.contrib import messages
from django.contrib.auth import (
    views as user_views, forms as user_forms, get_user_model)
from django.contrib.auth.tokens import default_token_generator
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.template import loader
from django.utils.http import int_to_base36, base36_to_int
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters

from .utils import send_password_reset_email


def login(request):
    return user_views.login(request, template_name="users/login.html")


def logout(request):
    return user_views.logout_then_login(request)





class PasswordResetForm(user_forms.PasswordResetForm):
    def save(self, request=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        UserModel = get_user_model()
        email = self.cleaned_data["email"]
        active_users = UserModel._default_manager.filter(
            email__iexact=email, is_active=True)
        for user in active_users:
            # Make sure that no email is sent to a user that actually has
            # a password marked as unusable
            if not user.has_usable_password():
                continue
            send_password_reset_email(user, request)


def password_reset(request):
    template_name = 'users/password_reset.html'

    if request.method == "POST":
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            form.save(request=request)
            messages.success(request, 'A password reset email has been sent to {0}'.format(
                form.cleaned_data['email']))
            return redirect(reverse(password_reset))
    else:
        form = PasswordResetForm()
    return render(request, template_name, {'form': form})


@sensitive_post_parameters()
@never_cache
def password_reset_set(request, uidb36=None, token=None):
    """
    View that checks the hash in a password reset link and presents a
    form for entering a new password.
    """
    UserModel = get_user_model()
    assert uidb36 is not None and token is not None  # checked by URLconf
    try:
        uid_int = base36_to_int(uidb36)
        user = UserModel._default_manager.get(pk=uid_int)
    except (ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is None or not default_token_generator.check_token(user, token):
        messages.error(request, (
            'The reset password link you used has expired. '
            'You must request another password reset email to continue'))
        return redirect(password_reset)

    validlink = True
    if request.method == 'POST':
        form = user_forms.SetPasswordForm(user, request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, (
                'Your password has been reset. '
                'You may now log in using your new password'))
            return redirect('dashboard')
    else:
        form = user_forms.SetPasswordForm(None)
    return render(request, 'users/password_reset_set.html', {'form': form})
