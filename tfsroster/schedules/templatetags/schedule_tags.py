import datetime

from collections import defaultdict
from django import template
from tfsroster.schedules.models import Availability, Day

register = template.Library()


def css(styles):
    return '; '.join('{0}:{1}'.format(*v) for v in styles.iteritems())


def seconds(time):
    return time.hour * 60 * 60 + time.minute * 60 + time.second


@register.filter
def by_days(schedule, dates):
    days_dict = {date: None for date in dates}
    days_dict.update({a.date: a for a in schedule.filter(date__in=(dates))})
    return [(date, days_dict.get(date, None)) for date in dates]


seconds_in_a_day = datetime.timedelta(days=1).total_seconds()


@register.filter
def position(availability):
    start = seconds(availability.start)
    end = seconds(availability.end)

    diff = (end - start) % seconds_in_a_day
    if diff == 0:
        diff = seconds_in_a_day

    left = "{:.6%}".format(start / seconds_in_a_day)
    width = "{:.6%}".format(diff / seconds_in_a_day)

    return css({
        'left': left,
        'width': width,
    })


class ScheduleNode(template.Node):
    TAG_NAME = 'schedule'
    END_TAG_NAME = 'end' + TAG_NAME

    def __init__(self, nodelist, station_var, daterange_var, shifts_var):
        self.nodelist = nodelist
        self.station_var = station_var
        self.daterange_var = daterange_var
        self.shifts_var = shifts_var

    def render(self, context):
        station = self.station_var.resolve(context)
        daterange = self.daterange_var.resolve(context)
        shifts = self.shifts_var.resolve(context)

        days = Day.objects\
            .filter(day__in=daterange, person__station=station)\
            .select_related('person')
        availabilities = Availability.objects\
            .filter(day__in=days)\
            .select_related('shift', 'day', 'day__person')

        availabilities_by_day = defaultdict(lambda: defaultdict(lambda: None))
        for a in availabilities:
            availabilities_by_day[a.day_id][a.shift] = a

        # schedule[person][day][1][shift] = availability
        schedule = defaultdict(lambda: defaultdict(
            lambda: (None, defaultdict(lambda: None))))
        for day in days:
            schedule[day.person][day.day] = (day, availabilities_by_day[day.pk])

        people = station.people.all().prefetch_related('qualifications', 'rank', 'phone_numbers')
        with context.push():
            context['_schedule'] = {
                'station': station, 'daterange': daterange, 'shifts': shifts}
            return ''.join(self.render_person(person, schedule[person], context)
                           for person in people)

    def render_person(self, person, row, context):
        with context.push():
            context['person'] = person
            context['_schedulerow'] = row
            return self.nodelist.render(context)

    @classmethod
    def handle(cls, parser, tokens):
        bits = tokens.split_contents()
        station_var = parser.compile_filter(bits[1])
        daterange_var = parser.compile_filter(bits[2])
        shifts_var = parser.compile_filter(bits[3])
        nodelist = parser.parse((cls.END_TAG_NAME,))
        parser.delete_first_token()
        return cls(nodelist, station_var, daterange_var, shifts_var)
register.tag(ScheduleNode.TAG_NAME, ScheduleNode.handle)


class ScheduleRowNode(template.Node):
    TAG_NAME = 'schedulerow'
    END_TAG_NAME = 'end' + TAG_NAME

    def __init__(self, nodelist):
        super().__init__()
        self.nodelist = nodelist

    def render(self, context):
        schedule = context['_schedule']
        days = context['_schedulerow']

        return ''.join(self.render_day(date, days[date], context)
                       for date in schedule['daterange'])

    def render_day(self, date, day, context):
        with context.push():
            context['date'] = date
            context['day'] = day[0]
            context['_scheduleday'] = day[1]
            return self.nodelist.render(context)

    @classmethod
    def handle(cls, parser, tokens):
        bits = tokens.split_contents()
        nodelist = parser.parse((cls.END_TAG_NAME,))
        parser.delete_first_token()
        return cls(nodelist)
register.tag(ScheduleRowNode.TAG_NAME, ScheduleRowNode.handle)


class ScheduleDayNode(template.Node):
    TAG_NAME = 'scheduleday'
    END_TAG_NAME = 'end' + TAG_NAME

    def __init__(self, nodelist):
        super().__init__()
        self.nodelist = nodelist

    def render(self, context):
        schedule = context['_schedule']
        shifts = schedule['shifts']
        availabilities = context['_scheduleday']

        return ''.join(self.render_availability(shift, availabilities[shift], context)
                       for shift in shifts)

    def render_availability(self, shift, availability, context):
        with context.push():
            context['shift'] = shift
            context['availability'] = availability
            return self.nodelist.render(context)

    @classmethod
    def handle(cls, parser, tokens):
        bits = tokens.split_contents()
        nodelist = parser.parse((cls.END_TAG_NAME,))
        parser.delete_first_token()
        return cls(nodelist)
register.tag(ScheduleDayNode.TAG_NAME, ScheduleDayNode.handle)
