window.TFS = window.TFS || {};
TFS.schedule_set_times = function(opts) {
	var availabilityAvailable = opts.available;
	var availabilityEmpty = opts.empty;

	$("[button~=checkbox]").on('change.button', function() {
		var button = $(this);
		var checkbox = button.find(":checkbox")
		button.toggleToken('button', 'selected', checkbox.is(":checked"));
	}).trigger("change.button");

	var scheduleRows = $('[data-availability-row]');
	scheduleRows.each(function() {
		var row = $(this);
		var shiftButtons = row.find('[button][shift]');
		var shiftCheckboxes = shiftButtons.find(':checkbox');
		var allDayButton = row.find('[data-all-day]');
		var unavailableCheckbox = row.find('[data-unavailable] :checkbox');
		var availabilitySelect = row.find('[data-availability]');

		var selectAllShifts = function() {
			shiftCheckboxes.prop('checked', true).trigger('change');
		};
		shiftCheckboxes.on('change', function() {
			defaultToAvailable();
			allDayButton.toggleToken('button', 'selected', shiftCheckboxes.filter(':checked').length == shiftButtons.length);
			unavailableCheckbox.prop('checked', !shiftCheckboxes.is(':checked')).trigger('change');
		});

		allDayButton.on('click', function(e) {
			selectAllShifts()
			defaultToAvailable();
			e.preventDefault();
		});

		var unavailableButton = row.find('[data-unavailable]');
		unavailableButton.on('click', function(e) {
			if (!$(this).is(':checked')) {
				shiftCheckboxes.prop('checked', false).trigger('change');
				availabilitySelect.val(availabilityEmpty);
			}
			e.preventDefault();
		});


		var defaultToAvailable = function() {
			console.log("Checking availability", availabilitySelect.val(), "vs", availabilityEmpty);
			if (availabilitySelect.val() === availabilityEmpty) {
				console.log("Setting to available");
				availabilitySelect.val(availabilityAvailable);
			}
		};

	});
};
