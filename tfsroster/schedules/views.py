import datetime

from contextlib import contextmanager

from datetime import timedelta as td

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.formtools.wizard.views import SessionWizardView
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.timezone import now as utcnow, localtime

from tfsroster.people.models import Station, Person

from .forms import SelectPersonForm, DayAvailabilityForm
from .models import Availability, Day, Shift
from .utils import daterange


def best_date():
    now = localtime(utcnow())
    today = now.date()
    if now.hour >= 17:
        return today + td(days=1)
    else:
        return today


def parse_date_or_best_date(value):
    """
    Attempt to parse the supplied date. If it fails, default to the best date
    to start on
    """
    try:
        week_start_datetime = datetime.datetime.strptime(value, '%Y-%m-%d')
        return week_start_datetime.date()
    except ValueError:
        return best_date()


@login_required
def pick_person(request, station_pk):
    station = get_object_or_404(Station, pk=station_pk)
    people = station.people.all().prefetch_related('qualifications', 'rank', 'phone_numbers')
    return render(request, 'schedules/set/person.html', {
        'people': people,
        'station': station})


@login_required
def set_schedule_for_person(request, station_pk, person_pk):
    station = get_object_or_404(Station, pk=station_pk)
    person = get_object_or_404(station.people.current(), pk=person_pk)

    week_start = parse_date_or_best_date(request.GET.get('week_start', ''))
    dates = list(daterange(week_start, week_start + td(days=14)))
    date_start = week_start
    date_end = dates[-1]

    next_week = week_start + td(days=7)
    previous_week = week_start - td(days=7)

    days = Day.objects\
        .filter(person=person, day__range=(date_start, date_end))\
        .select_related('availabilities')
    day_dict = {d.day: d for d in days}

    shifts = Shift.objects.all()
    day_forms = [DayAvailabilityForm(data=request.POST or None, person=person,
                                     shifts=shifts, day=date,
                                     instance=day_dict.get(date, None),
                                     prefix='date_' + date.isoformat())
                 for date in dates]

    next_person = station.people.current().successor_of(person)

    if request.method == 'POST':
        changed_forms = [form for form in day_forms if form.has_changed()]
        if all(form.is_valid() for form in changed_forms):
            new_schedule = [form.save() for form in changed_forms]
            messages.success(request,
                             "{0}'s schedule for {1} - {2} has been set".format(
                                 person, week_start, date_end))
            next_action = request.POST.get('next-action', None)
            if next_action == 'next-person' and next_person:
                return redirect(reverse(set_schedule_for_person, kwargs={
                    'station_pk': station_pk,
                    'person_pk': next_person.pk}))
            return redirect(reverse(pick_person, kwargs={
                'station_pk': station_pk}))

    return render(request, 'schedules/set/times.html', {
        'week_start': week_start,
        'date_end': date_end,
        'previous_week': previous_week,
        'next_week': next_week,
        'day_forms': day_forms,
        'available_status': Availability.STATUS_AVAILABLE,
        'person': person,
        'station': station,
        'shifts': shifts,
        'next_person': next_person})


@csrf_exempt  # TODO Fix up the view
@require_POST
@login_required
def toggle_rostered(request, station_pk, person_pk, availability_pk):
    station = get_object_or_404(Station, pk=station_pk)
    person = get_object_or_404(station.people.current(), pk=person_pk)
    availabilities = Availability.objects.filter(day__person=person)\
                                         .select_related('day')
    availability = get_object_or_404(availabilities, pk=availability_pk,
                                     day__person=person)

    if request.method == 'POST':
        availability.rostered = not availability.rostered
        availability.save()
        return render(request, 'schedules/update_availability_done.html', {
            'station': station,
            'shift': availability.shift,
            'day': availability.day,
            'person': person,
            'availability': availability})

    return render(request, 'schedules/update_availability.html', {
        'station': station,
        'person': person,
        'availability': availability})


@login_required
def view_schedule(request, station_pk):
    station = get_object_or_404(Station.objects.all(), pk=station_pk)

    week_start = parse_date_or_best_date(request.GET.get('week_start', ''))
    week_end = week_start + td(days=6)
    next_week = week_start + td(days=7)
    previous_week = week_start - td(days=7)
    shifts = list(Shift.objects.all())

    return render(request, 'schedules/view.html', {
        'week_start': week_start,
        'week_end': week_end,
        'week_range': list(daterange(week_start, next_week)),
        'previous_week': previous_week,
        'next_week': next_week,
        'shifts': shifts,
        'station': station})
