import datetime

from chainablemanager.manager import ChainableManager

from django.db import models


class Shift(models.Model):
    name = models.CharField(max_length=50)

    start = models.TimeField()
    end = models.TimeField()

    class Meta(object):
        ordering = ['start']

    def __str__(self):
        return self.name


class Day(models.Model):
    person = models.ForeignKey('people.Person', related_name='rostered_days')
    day = models.DateField()
    comments = models.TextField(blank=True)

    class Meta:
        unique_together = [
            ('person', 'day'),
        ]

    def __str__(self):
        return '{0} {1}'.format(self.day, self.person)

    def is_unavailable(self):
        return not self.availabilities.exists()


class AvailabilityManager(ChainableManager):
    class QuerySetMixin(object):
        pass

    def for_dates(self, person, dates):
        schedule = {s.date: s
                    for s in self.filter(person=person, date__in=dates)}

        for date in dates:
            if date in schedule:
                yield schedule[date]
            else:
                yield self.model(date=date, person=person)


class Availability(models.Model):
    day = models.ForeignKey(Day, related_name='availabilities')
    shift = models.ForeignKey(Shift, related_name='availabilities')

    STATUS_AVAILABLE = 'available'
    STATUS_WORKING = 'working'
    STATUS_ROSTERED = 'rostered'
    AVAILABILITY_CSS_CLASSES = {
        STATUS_AVAILABLE: 'good',
        STATUS_WORKING: 'warn',
    }
    AVAILABILITY_CHOICES = (
        (STATUS_AVAILABLE, 'Available'),
        (STATUS_WORKING, 'Working'),
    )
    availability = models.CharField(max_length=15, choices=AVAILABILITY_CHOICES)
    rostered = models.BooleanField(default=False)

    truck = models.ForeignKey('people.Truck', null=True, blank=True)

    objects = AvailabilityManager()

    class Meta(object):
        ordering = ('day__day', 'shift__start', '-rostered', 'availability')
        verbose_name_plural = 'Availabilities'

        unique_together = [
            ('day', 'shift'),
        ]

    def __str__(self):
        return '{person} - {date} {shift}: {status}'.format(
            person=self.day.person,
            date=self.day.day,
            shift=self.shift,
            status=self.get_status_display())

    @property
    def status(self):
        return self.STATUS_ROSTERED if self.rostered else self.availability

    def get_status_class(self):
        """Get a CSS class appropriate for the status"""
        css = 'rostered' if self.rostered else 'available'
        return css

    def get_availability_class(self):
        return self.AVAILABILITY_CSS_CLASSES[self.availability]


    def get_status_display(self):
        if self.rostered:
            return 'Rostered'
        else:
            return self.get_availability_display()
