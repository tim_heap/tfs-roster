# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Availability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('availability', models.CharField(choices=[('available', 'Available'), ('working', 'Working')], max_length=15)),
                ('rostered', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name_plural': 'Availabilities',
                'ordering': ('day__day', 'shift__start', '-rostered', 'availability'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Day',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('day', models.DateField()),
                ('comments', models.TextField(blank=True)),
                ('person', models.ForeignKey(related_name='rostered_days', to='people.Person')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Shift',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('start', models.TimeField()),
                ('end', models.TimeField()),
            ],
            options={
                'ordering': ['start'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='day',
            unique_together=set([('person', 'day')]),
        ),
        migrations.AddField(
            model_name='availability',
            name='day',
            field=models.ForeignKey(related_name='availabilities', to='schedules.Day'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='availability',
            name='shift',
            field=models.ForeignKey(related_name='availabilities', to='schedules.Shift'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='availability',
            name='truck',
            field=models.ForeignKey(blank=True, null=True, to='people.Truck'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='availability',
            unique_together=set([('day', 'shift')]),
        ),
    ]
