from django import forms
from django.contrib import admin
from djangosuggestions.widgets import SuggestionWidget
from tfsroster.schedules.models import (Shift, Availability)
from tfsroster.utils.admin import ExtraFieldsAdmin


@admin.register(Shift)
class ShiftAdmin(ExtraFieldsAdmin):
    list_display = ['name', 'start', 'end']


@admin.register(Availability)
class AvailabilityAdmin(ExtraFieldsAdmin):
    pass
