import datetime

from django import forms

from tfsroster.people.models import Person
from tfsroster.schedules.models import Shift, Day

from .models import Availability


class SelectPersonForm(forms.Form):

    person = forms.ModelChoiceField(Person.objects.all())

    def __init__(self, data=None, files=None, people=None, **kwargs):
        super(SelectPersonForm, self).__init__(data, files, **kwargs)
        if people is not None:
            self.fields['person'].queryset = people


class DayAvailabilityForm(forms.ModelForm):

    EMPTY = '-'

    shifts = forms.ModelMultipleChoiceField(
        required=False, queryset=Shift.objects.none(),
        widget=forms.CheckboxSelectMultiple)
    availability = forms.ChoiceField(choices=(
        ((EMPTY, '------------'),) + Availability.AVAILABILITY_CHOICES))
    unavailable = forms.BooleanField(required=False)

    class Meta:
        model = Day
        fields = ['comments']
        widgets = {'comments': forms.TextInput}

    def __init__(self, *args, instance, person, day, shifts, **kwargs):
        initial = {
            'availability': (
                instance.availabilities.all()[0].availability
                if instance is not None and instance.availabilities.all().exists()
                else self.EMPTY),
            'shifts': (
                [availability.shift for availability in instance.availabilities.all()]
                if instance is not None
                else []),
            'unavailable': instance.is_unavailable() if instance else False,
        }
        super(DayAvailabilityForm, self).__init__(
            *args, initial=initial, instance=instance, **kwargs)
        self.person = person
        self.day = day

        self.shifts = shifts
        self.shift_map = {str(shift.pk): shift for shift in shifts}
        self.fields['shifts'].queryset = shifts

    def clean(self):
        data = super().clean()
        if (data.get('availability') == self.EMPTY
                and data.get('shifts')):
            self.add_error('availability', 'Please select an availability')

        if data.get('unavailable') and data.get('shifts'):
            self.add_error(None, 'Can not be unavailable and have shifts')

        return data

    def save(self):
        instance = super(DayAvailabilityForm, self).save(commit=False)
        instance.person = self.person
        instance.day = self.day
        instance.save()

        selected_shifts = self.cleaned_data['shifts']
        availability = self.cleaned_data['availability']
        for shift in Shift.objects.all():
            if shift in selected_shifts:
                Availability.objects.update_or_create(
                    day=instance, shift=shift, defaults={
                        'availability': availability})
            else:
                Availability.objects.filter(day=instance, shift=shift).delete()

        return instance

    def is_unavailable(self):
        if self.instance.pk is None:
            return False
        return self.instance.is_unavailable()

    def is_all_day(self):
        if self.instance.pk is None:
            return False
        return self.instance.availabilities.count() == len(self.shifts)
