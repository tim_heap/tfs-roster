import datetime
import random

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import Group, Permission
from django.contrib.webdesign import lorem_ipsum

from tfsroster.constants import station_manager_group_name
from tfsroster.schedules.models import Shift, Availability, Day
from tfsroster.people.models import (
    Person, Station, Rank, Qualification, Truck, PhoneNumber)
from tfsroster.users.models import User
from tfsroster.constants import station_manager_group_name


class Command(BaseCommand):
    args = ''
    help = 'Generate some test data'

    def handle(self, *args, **options):

        station_manager = Group.objects.get(name=station_manager_group_name)

        admin = User.objects.create_superuser(email='admin@example.com', password='p')
        ueg = User.objects.create_user(email='chief@example.com', password='p')
        ueg.groups = [station_manager]
        uth = User.objects.create_user(email='ff@example.com', password='p')

        # These come from siteskeleton
        ranks = {rank.name: rank for rank in Rank.objects.all()}
        rc = ranks['Station cheif']
        r1 = ranks['1st officer']
        r2 = ranks['2nd officer']
        r3 = ranks['3rd officer']
        r4 = ranks['4th officer']
        rl = ranks['Leading firefighter']
        rf = ranks['Firefighter']
        rp = ranks['Probationary firefighter']
        rn = ranks['Provisionary firefighter']

        quals = {qual.code: qual for qual in Qualification.objects.all()}
        qf1 = quals['L1']
        qf2 = quals['L2']
        qcl = quals['C']
        qsr = quals['S']
        qdr = quals['DR']
        qmr = quals['MR']
        qba = quals['BA']
        qcs = quals['CSAW']
        qag = quals['GOOD']

        sl = Station.objects.create(name='Lenah Valley', number=9)
        sw = Station.objects.create(name='Wellington')
        sf = Station.objects.create(name='Fern Tree')
        so = Station.objects.create(name='Old Beach')
        sr = Station.objects.create(name='Risdon Vale')

        peg = Person.objects.create(name='Emma Gardner', rank=r1, station=sl, user=ueg)
        pcq = Person.objects.create(name='Collin Quon', rank=r2, station=sl)
        pev = Person.objects.create(name='Evan Cram', rank=r3, station=sl)
        pbp = Person.objects.create(name='Bronwen Pinkard', rank=r4, station=sl)
        pjc = Person.objects.create(name='Jimmy Collinson', rank=rl, station=sl)
        pkd = Person.objects.create(name='Kevin Donovan', rank=rf, station=sl)
        pth = Person.objects.create(name='Tim Heap', rank=rf, station=sl, user=uth)
        pnr = Person.objects.create(name='Nick Robinson', rank=rf, station=sl)
        pem = Person.objects.create(name='Emma Maingay', rank=rf, station=sl)
        pbm = Person.objects.create(name='Belle Monk', rank=rf, station=sl)

        peg.qualifications = [qcl, qdr, qba, qag]
        pcq.qualifications = [qcl, qdr, qmr, qag]
        pev.qualifications = [qf2, qdr, qmr, qag]
        pbp.qualifications = [qf2, qdr, qag]
        pjc.qualifications = [qf1, qdr]
        pkd.qualifications = [qsr, qdr, qag]
        pth.qualifications = [qf1, qag]
        pnr.qualifications = []
        pem.qualifications = [qf1, qdr, qag]
        pbm.qualifications = [qf1, qdr, qag]

        tl51 = Truck.objects.create(name='5.1', station=sl)
        tl52 = Truck.objects.create(name='5.2', station=sl)

        for person in Person.objects.all():
            for i in range(random.randint(1, 3)):
                mobile = random.choice([True, False])
                PhoneNumber.objects.create(
                    person=person,
                    number='0401 234 567' if mobile else '6234 5678',
                    type='Mobile' if mobile else 'Home',
                    primary=i == 1)

        today = datetime.date.today()
        shifts = list(Shift.objects.all())
        dates = list(map(lambda offset: today + datetime.timedelta(days=offset), range(14)))
        availabilities = []
        for person in Person.objects.all():
            for date in dates:
                if random.uniform(0, 1) < 0.1:
                    continue
                day = Day.objects.create(
                    person=person, day=date,
                    comments=(lorem_ipsum.words(random.randint(1, 3), common=False)
                              if random.uniform(0, 1) < 0.05 else ''))
                for shift in shifts:
                    if random.uniform(0, 1) < 0.1:
                        continue
                    availabilities.append(Availability(
                        day=day, shift=shift,
                        availability=(Availability.STATUS_AVAILABLE
                                      if random.uniform(0, 1) < 0.2
                                      else Availability.STATUS_WORKING)))

        Availability.objects.bulk_create(availabilities)
