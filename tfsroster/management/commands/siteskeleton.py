import datetime

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import Group, Permission

from tfsroster.constants import station_manager_group_name
from tfsroster.schedules.models import Shift
from tfsroster.people.models import Rank, Qualification
from tfsroster.constants import station_manager_group_name


class Command(BaseCommand):
    args = ''
    help = 'Generate data required for the site to work'

    def handle(self, *args, **options):
        Shift.objects.create(
            name='Morning', start=datetime.time(8), end=datetime.time(16))
        Shift.objects.create(
            name='Afternoon', start=datetime.time(16), end=datetime.time(22))
        Shift.objects.create(
            name='Night', start=datetime.time(22), end=datetime.time(8))

        pget = Permission.objects.get_by_natural_key

        station_manager = Group.objects.create(name=station_manager_group_name)
        station_manager.permissions = [
            pget('manage_own_station', 'people', 'station'),
            pget('add_person', 'people', 'person'),
            pget('change_person', 'people', 'person'),
            pget('delete_person', 'people', 'person'),
        ]

        manager = Group.objects.create(name='Administrator')
        manager.permissions = [
            pget('manage_all_stations', 'people', 'station'),
            pget('manage_own_station', 'people', 'station'),
            pget('add_person', 'people', 'person'),
            pget('change_person', 'people', 'person'),
            pget('delete_person', 'people', 'person'),
        ]

        rc = Rank.objects.create(name='Station cheif')
        r1 = Rank.objects.create(name='1st officer')
        r2 = Rank.objects.create(name='2nd officer')
        r3 = Rank.objects.create(name='3rd officer')
        r4 = Rank.objects.create(name='4th officer')
        rl = Rank.objects.create(name='Leading firefighter')
        rf = Rank.objects.create(name='Firefighter')
        rp = Rank.objects.create(name='Probationary firefighter')
        rn = Rank.objects.create(name='Provisionary firefighter')

        qf1 = Qualification.objects.create(
            name='Firefighter Level 1', code='L1', order=1,
            icon='static/css/img/qual-lvl1.png')
        qf2 = Qualification.objects.create(
            name='Firefighter Level 2', code='L2', order=2,
            icon='static/css/img/qual-lvl2.png')
        qcl = Qualification.objects.create(
            name='Crew leader', code='C', order=3,
            icon='static/css/img/qual-crew-leader.png')
        qsr = Qualification.objects.create(
            name='Supervise response', code='S', order=4,
            icon='static/css/img/qual-sector-commander.png')
        qdr = Qualification.objects.create(
            name='Drive in operational conditions', code='DR', order=5,
            icon='static/css/img/qual-drive.png')
        qmr = Qualification.objects.create(
            name='Medium rigid licence', code='MR', order=6,
            icon='static/css/img/qual-medium-rigid.png')
        qba = Qualification.objects.create(
            name='Breathing apparatus', code='BA', order=7,
            icon='static/css/img/qual-breathing-apparatus.png')
        qcs = Qualification.objects.create(
            name='Chainsaw', code='CSAW', order=8,
            icon='static/css/img/qual-chainsaw.png')
        qag = Qualification.objects.create(
            name='All good', code='GOOD', order=9,
            icon='static/css/img/qual-thumbs-up.png')
