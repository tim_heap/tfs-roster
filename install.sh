#!/usr/bin/env bash

# Quit when errors happen
set -e

# Helper to run a command as another user
function as () {
	local user="$1"
	shift
	sudo -u "$user" "$@"
}

function heading() {
	echo ''
	echo "$@"
	echo '--------------------------------------------'
}

v_home=~vagrant
p_name="tfsroster"
env="development"

# Path to the code
p_root="/vagrant"
# Where to put the venv
p_venv="${v_home}/venv"
# The name of the django directory
p_django=$p_name

# Database settings
db_name=$p_name
db_user=$p_name
db_pass="password"

# Python path to settings file
settings="settings"
# File path to settings file
settings_py="${p_root}/${settings//.//}.py"


heading "Built in 'STRAYA!"
sed -i 's/^# \(en_AU.UTF-8\)/\1/' /etc/locale.gen
locale-gen
# Use Australian mirrors for faster apt
cat >/etc/apt/sources.list <<APT
deb http://mirror.internode.on.net/pub/debian wheezy main
deb-src http://mirror.internode.on.net/pub/debian/ wheezy main

deb http://security.debian.org/ wheezy/updates main
deb-src http://security.debian.org/ wheezy/updates main
APT

heading 'Installing system packages'
# Update package listing
apt-get update -y
# Install packages required for deployment
apt-get install -y \
	build-essential \
	python3.2 python3.2-dev python3-pip \
	libjpeg-dev libtiff-dev zlib1g-dev libfreetype6-dev liblcms2-dev \
	postgresql libpq-dev
# Install packages required for developing
apt-get install -y git vim nano ncurses-term htop

# virtualenv global setup
pip-3.2 install virtualenv


# Set up the python environment
if ! [[ -e "${settings_py}" ]] ; then
	heading "Making ${settings_py}"
	cat > "${settings_py}" <<<"
from tfsroster.settings import *

DEBUG = True
TEMPLATE_DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '${db_name}',
        'USER': '${db_user}',
        'PASSWORD': '${db_pass}',
        'HOST': '127.0.0.1',
        'PORT': '31',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

SECRET_KEY = '$( cat /dev/urandom | base64 | head -c 60 )'

ASSETS_ROOT = path.join(path.dirname(__file__), 'var', 'assets')
STATIC_ROOT = path.join(ASSETS_ROOT, 'static')
MEDIA_ROOT = ASSETS_ROOT
"
fi

# Make local asset directories, required by STATIC_ROOT and MEDIA_ROOT above
mkdir -p \
	var \
	var/assets \
	var/assets/static \
	var/logs

heading 'Adding database user'
as postgres psql <<<"CREATE USER ${db_user} NOCREATEDB PASSWORD '${db_pass}'"

cd "${p_root}"
heading 'Creating virtual environment'
virtualenv ${p_venv}

heading 'Setting up bash environment'

# Set up bash to automatically activate the virtualenv
# This is handy when logging in
# Only needed in development
source_line="source '${v_home}/.bashrc.vagrant'"
if ! grep -qxF "${source_line}" < "${v_home}/.bashrc" ; then
	cat >>"${v_home}/.bashrc" <<<"${source_line}"
fi

# Automatically activate virtual environment
# Reset database state in one command
magic="${v_home}/.bashrc.vagrant"
cat >"${magic}" <<<"
cd '${p_root}'
[[ -e '${p_venv}/bin/activate' ]] && source '${p_venv}/bin/activate'
export DJANGO_SETTINGS_MODULE='${settings}'
function reset-db() {
	sudo -u postgres psql <<<\"DROP DATABASE ${p_name}\" && \\
	sudo -u postgres psql <<<\"CREATE DATABASE ${db_name} ENCODING='UTF8' LC_CTYPE='en_AU.UTF-8' LC_COLLATE='en_AU.UTF-8' TEMPLATE=template0\" && \\
	./manage.py migrate && \\
	./manage.py siteskeleton && \\
	./manage.py testdata
}"
chown vagrant:vagrant "${v_home}/.bashrc" "${magic}"
source "${v_home}/.bashrc.vagrant"

heading 'Installing python packages'
pip install -e . psycopg2

heading 'Resetting database'
reset-db
