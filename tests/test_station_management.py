import unittest

from django.contrib.auth.models import Group
from django.core import mail
from django.core.urlresolvers import reverse
from django.test import TestCase, RequestFactory

from tfsroster import constants
from tfsroster.users.models import User
from tfsroster.people.models import Station, Rank, Person
from tfsroster.people.forms import ChangePersonForm


class PersonManagementTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.group = Group.objects.create(name=constants.station_manager_group_name)
        self.rank = Rank.objects.create(name='Firefighter')
        self.station = Station.objects.create(name='Test station')

    def test_create_person(self):
        """
        Test creating a person without a login
        """
        request = self.factory.get(reverse(
            'person-create', kwargs=dict(station_pk=self.station.pk)))
        person = Person(station=self.station)
        data = {
            'name': 'Test user',
            'rank': str(self.rank.pk),
        }
        form = ChangePersonForm(data=data, instance=person)
        self.assertTrue(form.is_valid())
        form.save(request)

        self.assertEqual(Person.objects.all().count(), 1)
        person = Person.objects.get(name='Test user')
        self.assertIsNone(person.user)

    def test_create_person_with_user(self):
        """
        Test creating a person with a login
        """
        request = self.factory.get(reverse(
            'person-create', kwargs=dict(station_pk=self.station.pk)))
        person = Person(station=self.station)
        data = {
            'name': 'Test user',
            'rank': str(self.rank.pk),
            'has_login': '1',
            'user-email': 'test@example.com',
        }
        form = ChangePersonForm(data=data, instance=person)
        self.assertTrue(form.is_valid())
        form.save(request)

        self.assertEqual(Person.objects.all().count(), 1)
        self.assertEqual(User.objects.all().count(), 1)
        person = Person.objects.get(name='Test user')
        self.assertIsNotNone(person.user)
        self.assertFalse(person.user.has_usable_password())
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to[0], 'test@example.com')

    def test_edit_person(self):
        """
        Test editing a person without a login
        """
        person = Person.objects.create(station=self.station, name='Test user',
                                       rank=self.rank)
        request = self.factory.get(reverse('person-change', kwargs=dict(
            station_pk=self.station.pk, person_pk=person.pk)))
        data = {
            'name': 'Test user',
            'rank': str(self.rank.pk),
        }
        form = ChangePersonForm(data=data, instance=person)
        self.assertTrue(form.is_valid())
        form.save(request)

        self.assertEqual(Person.objects.all().count(), 1)
        self.assertEqual(User.objects.all().count(), 0)
        person = Person.objects.get(name='Test user')
        self.assertIsNone(person.user)
        self.assertEqual(len(mail.outbox), 0)

    def test_edit_person_add_account(self):
        """
        Test editing a person can add a login
        """
        person = Person.objects.create(station=self.station, name='Test user',
                                       rank=self.rank)
        request = self.factory.get(reverse('person-change', kwargs=dict(
            station_pk=self.station.pk, person_pk=person.pk)))
        data = {
            'name': 'Test user',
            'rank': str(self.rank.pk),
            'has_login': '1',
            'user-email': 'test@example.com',
        }
        form = ChangePersonForm(data=data, instance=person)
        self.assertTrue(form.is_valid())
        form.save(request)

        self.assertEqual(Person.objects.all().count(), 1)
        self.assertEqual(User.objects.all().count(), 1)
        person = Person.objects.get(name='Test user')
        self.assertIsNotNone(person.user)
        self.assertFalse(person.user.has_usable_password())
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to[0], 'test@example.com')

    def test_edit_person_with_account(self):
        """
        Test editing a person with a login keeps the login
        """
        user = User.objects.create_user(email='test@example.com', password='p')
        person = Person.objects.create(station=self.station, name='Test user',
                                       rank=self.rank, user=user)
        request = self.factory.get(reverse('person-change', kwargs=dict(
            station_pk=self.station.pk, person_pk=person.pk)))
        data = {
            'name': 'Test user',
            'rank': str(self.rank.pk),
            'has_login': '1',
            'user-email': 'test@example.com',
        }
        form = ChangePersonForm(data=data, instance=person)
        self.assertTrue(form.is_valid())
        form.save(request)

        self.assertEqual(Person.objects.all().count(), 1)
        self.assertEqual(User.objects.all().count(), 1)
        person = Person.objects.get(name='Test user')
        self.assertIsNotNone(person.user)
        self.assertTrue(person.user.has_usable_password())
        self.assertEqual(len(mail.outbox), 0)

    def test_edit_person_remove_account(self):
        """
        Test editing a person with a login allows deleting the login
        """
        user = User.objects.create_user(email='test@example.com', password='p')
        person = Person.objects.create(station=self.station, name='Test user',
                                       rank=self.rank, user=user)
        request = self.factory.get(reverse('person-change', kwargs=dict(
            station_pk=self.station.pk, person_pk=person.pk)))
        data = {
            'name': 'Test user',
            'rank': str(self.rank.pk),
        }
        form = ChangePersonForm(data=data, instance=person)
        self.assertTrue(form.is_valid())
        form.save(request)

        self.assertEqual(Person.objects.all().count(), 1)
        self.assertEqual(User.objects.all().count(), 0)
        person = Person.objects.get(name='Test user')
        self.assertIsNone(person.user)
        self.assertEqual(len(mail.outbox), 0)
