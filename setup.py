#!/usr/bin/env python

import ast
import sys
import os

from setuptools import setup, find_packages


with open('README.rst', 'r') as f:
    readme = f.read()

with open('tfsroster/version.py') as f:
    exec(f.read())

setup(
    name='tfsroster',
    version=version,
    description='Roster application for TFS Volunteer Firefighters',
    long_description=readme,

    author='Tim Heap',
    author_email='tim@timheap.me',
    url='http://timheap.me/',

    packages=find_packages(),
    include_package_data=True,

    license='BSD',
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Framework :: Django',
    ],

    install_requires=[
        "Django>=1.7,<1.8",
        "django-admin-extensions==0.7.1",
        "django-chainable-manager==0.5.0",
        "django-featured-item==0.4.1",
        "django-filemanager-tjh==0.6.0",
        "django-localflavor-au-tjh==1.1.2",
        "django-positions==0.5.2",
        "django-suggestions==0.2.0",
        "django-template-media==0.3.0",
        "pillow==2.5.3",

    ],
    zip_safe=False,
)
