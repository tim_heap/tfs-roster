======================================
Tasmania Fire Service Volunteer Roster
======================================

A rostering application to help manage volunteer firefighters during busy fire
seasons.

Deploying
=========

Assuming you are deploying using Postgres and uWSGI, install the required
Python packages::

    $ pip install uwsgi psycopg2 tfsroster

Create a ``settings.py`` file that defines at least the following:

.. code:: python

    from tfsroster.settings.production import *

    # Who to email when things break.
    ADMINS = [
        ('Your name', 'you@example.com'),
    ]

    # Make this unique, and don't share it with anybody.
    SECRET_KEY = 'Secret key!'

    # The domain name this application will be served from
    ALLOWED_HOSTS = ['example.com']

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'tfsroster',
            'USER': 'username',
            'PASSWORD': 'password',
            'HOST': 'localhost',
            'PORT': '',
        }
    }

    # How to send emails. A properly configured email server is required to
    # send emails that don't bounce.
    # https://docs.djangoproject.com/en/1.7/topics/email/#email-backends
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

    # The from address used to send emails. An invalid address may cause
    # emails to bounce from strict email hosts.
    DEFAULT_FROM_EMAIL = 'from@example.com'

    # The location to save static and uploaded files to.
    # The use running the application must have write access to MEDIA_ROOT
    # for uploads to work.
    MEDIA_ROOT = '/path/to/assets/media/'
    STATIC_ROOT = '/path/to/assets/static/'
    FILEMANAGER_UPLOAD_ROOT = MEDIA_ROOT

Other settings you may want to define include:

.. code:: python

    # Enable this if things break
    DEBUG = True

Next, set up some environment variables:

.. code:: sh

    # Settings module is at e.g. /etc/www/tfsroster/settings.py
    export PYTHONPATH="/etc/www/tfsroster:$PYTHONPATH"
    export DJANGO_SETTINGS_MODULE=settings

Run the following to initialise the database and copy all static assets into
``STATIC_ROOT``. These commands should be run after each install or upgrade of
the package.

.. code:: sh

    django-admin migrate
    django-admin collectstatic

Finally, run ``uwsgi``:

.. code:: sh

    uwsgi --master --module=tfsroster.wsgi:application

You will probably want to add some more settings, such as ``--socket`` so you
can connect to the uwsgi process from your webserver.
